import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();


// class Car {
//     constructor(name) {
//         this.brand = name;
//     }

//     present() {
//         return 'haha: ' + this.brand;
//     }
// };

// const mycar = new Car('Lan');
// // mycar.present();

// ReactDOM.render(mycar.present(), document.getElementById('root'));
// // ReactDOM.render(<p>Hello</p>, document.getElementById('root'));


// //state
// class Car extends React.Component {
//     constructor() {
//         super();
//         this.state = {color: 'red'};
//     }
//     render() {
//         return <h2>haha {this.state.color} ne!</h2>
//     }
// }

import Car from './App.js'

class Garage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hi: 'stork dep zai',
            age: 12,
            love: 'nolove',
            show: 'hide'
        }
    }
    
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                show: 'hide'
            })
        }, 4000)
    }

    changeColor = () => { 
        this.setState({
            hi: 'hh cc',
            age: 40,
            love: 'islove',
            status: 'smile',
            show: 'show'
        });
        console.log(this);  
    }

    static getDerivedStateFromProps(props, state) {
        return {hi: props.hilan}
    }

    render() {
        return (
            <div>
                <h1>stork {this.props.brand} haha</h1>
                <p>tao ne {this.state.hi}</p>
                <p>tuoi {this.state.age}</p>
                <p>love {this.state.love}</p>
                <p className={this.state.show}>status: {this.state.status}</p>

                <button
                    type="button"
                    onClick={this.changeColor}
                >Change age</button>
                
                <Car />
            </div>
            
            
        );

    }
}

// const storksay = <Garage brand="chu di dau"/>

ReactDOM.render(<Garage brand="smart" hilan="lan oi ne ba !" />, document.getElementById('root'));

// props

// class Car extends React.Component {
//     // constructor() {
//     //     super();
//     //     this.props = {color: 'blue'};
//     // }
//     render() {
//         return <h1>haho {this.props.color} hoho!</h1>
//     }
// }

// ReactDOM.render(<Car color='ohoho'/>, document.getElementById('root'));

// test new branch